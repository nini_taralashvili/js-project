// calculating the highest GPA with switch

let credits=4+2+7+5;
let gpa_Neo = ((60.2*4) + (57.3*2) + (72.4*7) + (88*5))/credits;

let gpa_Indiana = ((78.2*4) + (52.3*2) + (66.4*7) + (80*5))/credits;

let gpa_Severusa= ((75.2*4) + (67.3*2) + (54.4*7) + (90*5))/credits;

let gpa_Aladini = ((80.2*4) + (52.3*2) + (68.4*7) + (76*5))/credits;


let maxGpa;
maxGpa=gpa_Neo;

switch(true){
    case(maxGpa < gpa_Indiana):
    maxGpa=gpa_Indiana;
    console.log("Indiana has the highest GPA "  + maxGpa);
    break;

    case(maxGpa < gpa_Severusa):
    maxGpa=gpa_Severusa;
    console.log("Severusa has the highest GPA "  + maxGpa);
    break;

    case(maxGpa < gpa_Aladini):
    maxGpa=gpa_Aladini;
    console.log("Aladini has the highest GPA "  + maxGpa);
    break;

    default:
        console.log("Neo has the highest GPA " + maxGpa);
}


// calculating the highest points with switch

let points_Of_Neo = 60.2 + 57.3 + 72.4 + 88;
let points_Of_Indiana = 78.2 + 52.3 + 66.4 + 80;
let points_Of_Severusa = 75.2 + 67.3 + 54.4 + 90;
let points_Of_Aladini = 80.2 + 52.3 + 68.4 +76;

let maxPoints;
maxPoints=points_Of_Neo;

switch(true){
    case(maxPoints < points_Of_Indiana):
    maxPoints = points_Of_Indiana;
    console.log("Indiana has the highest points "  + maxPoints);
    break;

    case(maxPoints < points_Of_Severusa):
    maxPoints=points_Of_Severusa;
    console.log("Severusa has the highest points "  + maxPoints);
    break;

    case(maxPoints < points_Of_Aladini):
    maxPoints=points_Of_Aladini;
    console.log("Aladini has the highest points "  + maxPoints);
    break;

    default:
        console.log("Neo has the highest points " + maxPoints);
}