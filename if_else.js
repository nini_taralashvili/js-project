// calculating the highest GPA with if-else
let credits=4+2+7+5;
let gpaNeo = ((60.2*4) + (57.3*2) + (72.4*7) + (88*5))/credits;

let gpaIndiana = ((78.2*4) + (52.3*2) + (66.4*7) + (80*5))/credits;

let gpaSeverusa= ((75.2*4) + (67.3*2) + (54.4*7) + (90*5))/credits;

let gpaAladini = ((80.2*4) + (52.3*2) + (68.4*7) + (76*5))/credits;


let max_gpa;
max_gpa=gpaNeo;
if(max_gpa < gpaIndiana){
    max_gpa=gpaIndiana;
    console.log( "Indiana has the highest GPA " + max_gpa);

}
else if(max_gpa < gpaSeverusa){
    max_gpa=gpaSeverusa;
    console.log( "Severusa has the highest GPA "+ max_gpa);

}
else if(max_gpa < gpaAladini){
    max_gpa=gpaAladini;
    console.log("Aladini has the highest GPA " + max_gpa);
}
else{
    console.log("Neo has the highest GPA "+ max_gpa);
}

//calculating the highest points with if-else

let pointsOfNeo = 60.2 + 57.3 + 72.4 + 88;
let pointsOfIndiana = 78.2 + 52.3 + 66.4 + 80;
let pointsOfSeverusa = 75.2 + 67.3 + 54.4 + 90;
let pointsOfAladini = 80.2 + 52.3 + 68.4 +76;


let max_points;
max_points=pointsOfNeo;

if(max_points < pointsOfIndiana){
    max_points=pointsOfIndiana;
    console.log( "Indiana has the highest points " + max_points);

}
else if(max_points< pointsOfSeverusa){
    max_points=pointsOfSeverusa;
    console.log( "Severusa has the highest  points "+ max_points);

}
else if(max_points < pointsOfAladini){
    max_points=pointsOfAladini;
    console.log("Aladini has the highest points " + max_points);
}
else{
    console.log("Neo has the highest points "+ max_points);
}
